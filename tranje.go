package main

import (
	"flag"
	"fmt"
	"html"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"regexp"
)

func version() {
	os.Stderr.WriteString(`tranje: v0.1.0
`)
}

func usage() {
	os.Stderr.WriteString(`Mutual conversion between English and Japanese
Usage: tranje [OPTION] [INPUT_FILE]

Options: -t is necessary
	-t =(ja|en)   output of language(ja or en)
	--help        show this help message
	--version     print the version

Report bugs to <kusabashira227@gmail.com>
`)
}

var translatorURL = `http://www.excite.co.jp/world/english/`

var textExtractor = regexp.MustCompile(`<textarea.+after.+>(.*)</textarea>`)

func extractResult(rawhtml string) (string, error) {
	m := textExtractor.FindStringSubmatch(rawhtml)
	if len(m) < 2 {
		return "", fmt.Errorf("parse error")
	}
	
	textAreaText := m[1]
	return html.UnescapeString(textAreaText), nil
}

func translate(text, outputLanguage string) (string, error) {
	values := url.Values{}
	values.Add("before", text)
	values.Add("wb_lp", outputLanguage)

	res, err := http.PostForm(translatorURL, values)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}

	return extractResult(string(body))
}

func _main() (exitCode int, err error) {
	var (
		toLanguage = flag.String("t", "", "output language(ja or en)")
		isHelp     = flag.Bool("help", false, "show this help message")
		isVersion  = flag.Bool("version", false, "print the version")
	)
	flag.Parse()

	if *isHelp {
		usage()
		return 0, nil
	}

	if *isVersion {
		version()
		return 0, nil
	}

	inFile := os.Stdin
	if 0 < flag.NArg() {
		file := flag.Arg(0)
		inFile, err = os.Open(file)
		if err != nil {
			return 1, err
		}
	}

	var input string
	in, err := ioutil.ReadAll(inFile)
	if err != nil {
		return 1, err
	}
	input = string(in)

	var outputLanguage string
	switch *toLanguage {
	case "ja":
		outputLanguage = "ENJA"
	case "en":
		outputLanguage = "JAEN"
	default:
		return 1, fmt.Errorf("-t: illegal specified: %v", *toLanguage)
	}

	out, err := translate(input, outputLanguage)
	if err != nil {
		return 1, err
	}
	fmt.Print(out)

	return 0, nil
}

func main() {
	exitCode, err := _main()
	if err != nil {
		fmt.Fprintln(os.Stderr, "error: ", err)
	}
	os.Exit(exitCode)
}
