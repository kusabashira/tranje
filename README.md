tranje
===
Mutual conversion between English and Japanese 
with [excite translator](http://www.excite.co.jp/world/)

Usage
------
	Usage: tranje [OPTION] [INPUT_FILE]

	Options: -t is necessary
		-t =(ja|en)   language of the target(ja or en)
		--help        show this help message
		--version     print the version

Example
--------
	$ cat hello.txt
	こんにちは、世界

	$ cat hello.txt | tranje -t en 
	Hello, world 

	$ cat usage.txt
	Mutual conversion between English and Japanese 
	with [excite translator](http://www.excite.co.jp/world/)
	
	$ tranje -t ja usage.txt
	英語と日本語の間の相互の転換‥‥で
	[翻訳者を興奮させる](http://www.excite.co.jp/world/)

License
--------
MIT License

Author
-------
Kusabashira <kusabashira227@gmail.com>
